class Labrador
  LABRADOR_SET_DEVICE_MODE = 2147765248
  LABRADOR_SET_PSU = 2147634177
  LABRADOR_SET_DIGITAL_PINS = 2147634178
  LABRADOR_ALLOC_BUFFERS = 3221769226

  def self.open(device)
    io = File.open(device)
    begin
      yield new(io)
    ensure
      io.close
    end
  end

  def initialize(io)
    @io = io
  end

  def ioctl(cmd, value)
    @io.ioctl(cmd, value)
  end

  def set_pins(state)
    ioctl(LABRADOR_SET_DIGITAL_PINS, [state].pack('S'))
  end

  def set_psu(value)
    ioctl(LABRADOR_SET_PSU, [value].pack('S'))
  end

  def set_device_mode(mode, gain_mask)
    ioctl(LABRADOR_SET_DEVICE_MODE, [mode, gain_mask].pack('SS'))
  end

  def alloc_buffers(num_bufs)
    arg = [num_bufs, 0].pack('LL')
    ioctl(LABRADOR_ALLOC_BUFFERS, arg)
    num_bufs, map_size = arg.unpack('LL')
    return {num_bufs: num_bufs, map_size: map_size}
  end
end

if __FILE__ == $0
  Labrador.open("/dev/labrador0.0") do |lab|
    lab.set_device_mode(0, 7196)
    lab.set_psu(84) # 12 V

    x = lab.alloc_buffers(20)
    p x

    #state = 0b0000
    #loop do
    #  lab.set_pins(state)
    #  state ^= 0b1001
    #  sleep 1
    #end
  end
end
