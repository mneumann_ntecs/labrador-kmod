#include <stdio.h>
#include "src/labrador_ioctl.h"

int main(int argc, char **argv) {
	printf("LABRADOR_SET_DEVICE_MODE: %lu\n", LABRADOR_SET_DEVICE_MODE);
	printf("LABRADOR_SET_PSU: %lu\n", LABRADOR_SET_PSU);
	printf("LABRADOR_SET_DIGITAL_PINS: %lu\n", LABRADOR_SET_DIGITAL_PINS);
	printf("LABRADOR_ALLOC_BUFFERS: %lu\n", LABRADOR_ALLOC_BUFFERS);
	return 0;
}
