#include "src/labrador_ioctl.h"
#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

void hexdump(unsigned char *buf, int len) {
	for (int i = 0; i < len; ++i) {
		if ((i & 0x0F) == 0)
			printf("\n");
		printf(" %02x", buf[i]);
	}
	printf("\n");
}
void hexdump_data(unsigned char *buf, int len) {
	for (int i = 0; i < len; ++i) {
		if ((i & 0x0F) == 0)
			printf("\n");
		// XXX: This depends on AC or DC channel
		// 0x80 = 0b10000000 = -127
		// 0x7F = 0b01111111 = 127
		int v = ((signed char)buf[i]);
		printf(" % 3d", v);
	}
	printf("\n");
}


int main(int argc, char **argv) {
	int fd, error;
	struct labrador_alloc_buffers alloc_bufs;
	unsigned char *buf;

	error = 0;

	fd = open("/dev/labrador0.0", O_RDONLY);
	if (fd == -1) {
		perror("open");
		return 1;
	}

	printf("size: %ld\n", sizeof(struct labrador_fgen_data));

	// Set the function generator
	struct labrador_fgen_data fgen;
	fgen.channel = LABRADOR_FGEN_CH1;
	// This should roughly give 1/2 Hz
	//fgen.timer_period = 11718;
	//fgen.clock_setting = 5;

	// much faster freqency
	fgen.timer_period = 400;
	fgen.clock_setting = 1;
	fgen.n_samples = 512;
	// the samples are scaled on the uC
	//
	// On the AC fgen channel:
	// i.e. when I specify samples in the range of [200, 250],
	// the amplitude will be "50" and "200" corresponds to -25,
	// and "250" to +25.
	//
	// On the DC fgen channel:
	// Same for the amplitude, i.e. if my samples are in the
	// range [50, 100], then the amplitude will be "50". 
	//
	for (int i = 0; i < 512; ++i) {
		// 0 = negative max
		// 255 = positive max
		fgen.samples[i] = i < 256 ? 0 : 50;
	}

	error = ioctl(fd, LABRADOR_SET_FGEN, &fgen);
	if (error == -1) {
		perror("fgen");
		return 1;
	}

	struct labrador_device_mode dev_mode;
	dev_mode.mode = 2;
	dev_mode.gain_mask = 2000; // 256; // 7196;
	error = ioctl(fd, LABRADOR_SET_DEVICE_MODE, &dev_mode);
	if (error == -1) {
		perror("set device mode");
		return 1;
	}

	alloc_bufs.num_bufs = 30;
	error = ioctl(fd, LABRADOR_ALLOC_BUFFERS, &alloc_bufs);
	if (error == -1) {
		perror("ioctl");
		return 1;
	}
	printf("num_bufs: %d, map_size: %d\n", alloc_bufs.num_bufs, alloc_bufs.map_size);

	buf = (unsigned char *) mmap(NULL, (size_t)alloc_bufs.map_size, PROT_READ, MAP_SHARED, fd, 0);
	if (buf == MAP_FAILED) {
		perror("mmap");
		return 1;
	}

	printf("mapped %p\n", buf);


	error = ioctl(fd, LABRADOR_START_ISOC);
	if (error == -1) {
		perror("start isoc");
		return 1;
	}

	while (1) {
		printf("\n");
		hexdump(buf, 8 /*64*/);
		printf("\n");
		for (int i = 0; i < 30; ++i)
			hexdump_data(buf+(i*750) + 64, 375);
		sleep(1);
	}

	return 0;
}
