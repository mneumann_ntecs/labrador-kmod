/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Michael Neumann
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef _LABRADOR_IOCTL_H
#define _LABRADOR_IOCTL_H

#include <sys/stdint.h>
#include <sys/ioccom.h>

struct labrador_device_mode {
	uint16_t mode;
	uint16_t gain_mask;
} __packed;

struct labrador_alloc_buffers {
	/* The number of buffers to allocate. IN/OUT */
	uint32_t num_bufs;
	/* On success, the size of the memory region to map. OUT */
	uint32_t map_size;
} __packed;

enum LABRADOR_FGEN_CHANNEL {
	LABRADOR_FGEN_CH1 = 0,
	LABRADOR_FGEN_CH2 = 1,
};

/*
 * Set the function generator
 */
struct labrador_fgen_data {
	/*
	 * 0 = channel1
	 * 1 = channel2
	 */
	uint16_t	channel;

	uint16_t	timer_period;
	uint16_t	clock_setting;

	/* Number of samples */
	uint16_t	n_samples;

	/*
	 * Array of 8-bit samples used to create the waveform
	 * Only `n_samples` are taken.
	 */
	uint8_t	 samples[512];
} __packed;

#define LABRADOR_SET_DEVICE_MODE	_IOW('L', 0, struct labrador_device_mode)
#define LABRADOR_SET_PSU 		_IOW('L', 1, uint16_t)
#define LABRADOR_SET_DIGITAL_PINS	_IOW('L', 2, uint16_t)
#define LABRADOR_SET_FGEN		_IOW('L', 3, struct labrador_fgen_data)
#define LABRADOR_START_ISOC 		_IO('L', 4)

#define LABRADOR_ALLOC_BUFFERS		_IOWR('L', 10, struct labrador_alloc_buffers)

#endif
