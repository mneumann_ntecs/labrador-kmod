/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Michael Neumann
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include <sys/stdint.h>
#include <sys/param.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <sys/systm.h>
#include <sys/kernel.h>
#include <sys/bus.h>
#include <sys/module.h>
#include <sys/lock.h>
#include <sys/condvar.h>
#include <sys/sysctl.h>
#include <sys/unistd.h>
#include <sys/malloc.h>
#include <sys/priv.h>
#include <sys/conf.h>
#include <sys/fcntl.h>
#include <vm/vm.h>
#include <vm/pmap.h>
#include <sys/mman.h>


#ifdef __FreeBSD__
#include <sys/mutex.h>
#include <dev/usb/usb.h>
#include <dev/usb/usbdi.h>
#include "usbdevs.h"

#define	USB_DEBUG_VAR	usb_debug
#include <dev/usb/usb_debug.h>

#define PRINTF printf
#define SNPRINTF snprintf
#define FREE free
#define LAB_LOCK(lk) mtx_lock(lk)
#define LAB_UNLOCK(lk) mtx_unlock(lk)
#define LAB_LOCK_INIT(lk, name) mtx_init(lk, name, NULL, MTX_DEF)
#define LAB_LOCK_UNINIT(lk) mtx_destroy(lk)
#define LAB_LOCK_DEF struct mtx
#endif

#ifdef __DragonFly__
#include <bus/u4b/usb.h>
#include <bus/u4b/usbdi.h>
#include "usbdevs.h"

#define	USB_DEBUG_VAR	usb_debug
#include <bus/u4b/usb_debug.h>

#include <sys/sysent.h>
#include <sys/devfs.h>

#define PRINTF kprintf
#define SNPRINTF ksnprintf
#define FREE kfree

#define LAB_LOCK_INIT(lk, name) lockinit(lk, name, 0, LK_CANRECURSE)
#define LAB_LOCK_UNINIT(lk) lockuninit(lk)

#define LAB_LOCK(lk) lockmgr(lk, LK_EXCLUSIVE)
#define LAB_UNLOCK(lk) lockmgr(lk, LK_RELEASE)
#define LAB_LOCK_DEF struct lock

#endif

#include "labrador_ioctl.h"

#define ESPOTEK_VENDOR			0x03eb
#define ESPOTEK_LABRADOR		0xba94

#define LAB_CMD_SET_FGEN_WAVEFORM_CH2	0xa1
#define LAB_CMD_SET_FGEN_WAVEFORM_CH1	0xa2
#define LAB_CMD_SET_PSU 		0xa3
/*
 * If function generator amplitude + offset >= 3.2 V:
 * - FGEN TRIPLE is 1 (CH2)
 * - FGEN TRIPLE is 2 (CH1)
 * - FGEN TRIPLE is 3 (CH1+CH2)
 */
#define LAB_CMD_SET_FGEN_TRIPLE		0xa4
#define LAB_CMD_SET_DEVICE_MODE		0xa5
#define LAB_CMD_SET_DIGITAL_PINS	0xa6
#define LAB_CMD_GET_FIRMWARE_VERSION	0xa8
#define LAB_CMD_GET_FIRMWARE_VARIANT	0xa9
#define LAB_CMD_START_ISOC		0xaa

enum {
	LAB_ISOC0_RD = 0,
};

enum LAB_MODE {
	/* mode=0: Oscilloscope CH1 @ 375 kHZ (8-bit) */
	LAB_MODE_CH1 = 0,

	/* mode=1: ??? Oscilloscope CH2 @ 375 kHZ (8-bit) */
	LAB_MODE_CH2 = 1,

	/* mode=2: Oscilloscope CH1+CH2 @ each 375 kHZ (8-bit) */
	LAB_MODE_CH1_CH2 = 2,

	/* mode=3: Logic Analyser CH1 @ 3 MHz (1-bit) */
	LAB_MODE_LOGIC_CH1 = 3,

	/* mode=4: Logic Analyser CH1+CH2 @ each 3 MHz (1-bit) */
	LAB_MODE_LOGIC_CH1_CH2 = 4,

	/* mode=5: off */
	LAB_MODE_OFF = 5,

	/* mode=6: Oscilloscope CH1 @ 750 kHZ (8-bit) */
	LAB_MODE_CH1_DSR = 6,

	/* mode=7: Multimeter CH1 @ 375 kHZ (12-bit) */
	LAB_MODE_CH1_MULTI = 7,
};

#define LAB_ENDPOINTS 1

struct measurements_header {
	uint64_t total_bytes;

	/*
	 * The byte position of the last written byte.
	 *
	 * -1 indicating that we never have written anything.
	 *
	 * This gets updated when we receive an ISOC frame.
	 */
	uint64_t current_position;

	/*
	 * A flag indicating that we are currently writing to
	 * that buffer.
	 */
	uint64_t busy;

	/*
	 * A flag indicating that we are currently capturing data.
	 */
	uint64_t capturing;

	/* Total number of blocks */ 
	uint64_t num_blocks;

	/* The size of each block (should be 750 bytes) */
	uint64_t block_size;

	/* To make it 64 bytes / a cache line */
	uint64_t _reserved[2];
} __packed;

/*
 * Measurement data arrives in "blocks" of size 750.
 *
 * We store these in a circular buffer which can be 
 * memory mapped by a user.
 *
 * The memory organization is:
 *
 * 	struct measurements_header: 64 bytes
 * 	N * 750 bytes of measurement data
 */
struct measurements {
	/*
	 * A pointer to the allocated memory buffer
	 * where both the `struct measurements_header`
	 * resides, followed by actual measurement data.
	 */
	uint8_t *m_buffer;

	/*
	 * The number of 750-bytes buffers that we have allocated
	 */
	uint32_t m_num_bufs;

	/*
	 * The total size of the measurements buffer.
	 */
	uint32_t m_size;
};

struct labrador_softc {
	struct usb_device *sc_udev;
	uint32_t sc_unit;
	uint8_t	sc_name[16];
	uint8_t sc_iface_index;

	LAB_LOCK_DEF sc_lock;
	struct usb_xfer *sc_xfer[LAB_ENDPOINTS];
	struct cdev *sc_cdev;

	/*
	 * Holds the thread that is currently connected to
	 * the device node.
	 */
	struct thread *sc_connected_td;

	/*
	 * Holds information about allocated buffers where we write
	 * measurement data to.
	 */
	struct measurements sc_measurements;

	uint16_t firmware_version;
	uint8_t  firmware_variant;
};

/* prototypes */

static device_probe_t labrador_probe;
static device_attach_t labrador_attach;
static device_detach_t labrador_detach;
static devclass_t labrador_devclass;

static d_open_t		labrador_open;
static d_close_t	labrador_close;
static d_ioctl_t	labrador_ioctl;
static d_mmap_t		labrador_mmap;

static void
labrador_isoc_callback(struct usb_xfer *xfer, usb_error_t error);

static int labrador_send_control(struct labrador_softc *sc,
		uint8_t request_type,
		uint8_t request,
		uint16_t value,
		uint16_t index,
		uint16_t length,
		uint8_t *data);
static int
labrador_cmd(struct labrador_softc *sc, uint8_t cmd, uint16_t value, uint16_t index);

static int
labrador_get_firmware_version(struct labrador_softc *sc, uint16_t *firmver);
static int
labrador_get_firmware_variant(struct labrador_softc *sc, uint8_t *firmvar);
static int
labrador_set_psu(struct labrador_softc *sc, uint16_t duty_psu);
static int
labrador_start_isoc(struct labrador_softc *sc);
static int
labrador_set_device_mode(struct labrador_softc *sc, uint16_t mode, uint16_t gain_mask);
static int
labrador_set_digital_pins(struct labrador_softc *sc, uint16_t state);
static int
labrador_set_fgen(struct labrador_softc *sc, struct labrador_fgen_data *fgen);

static int
labrador_alloc_buffers(struct labrador_softc *sc, struct labrador_alloc_buffers *alloc_bufs);


static device_method_t labrador_methods[] = {
	DEVMETHOD(device_probe, labrador_probe),
	DEVMETHOD(device_attach, labrador_attach),
	DEVMETHOD(device_detach, labrador_detach),
	DEVMETHOD_END
};

#if defined(__FreeBSD__)
static struct cdevsw labrador_cdevsw = {
	.d_version = D_VERSION,
	.d_name = "labrador",
	.d_flags = D_TRACKCLOSE,
#elif defined(__DragonFly__)
static struct dev_ops labrador_cdevsw = {
	{ "labrador", 0, D_TRACKCLOSE | D_MPSAFE },
#else
#error "Only build on FreeBSD or DragonFly"
#endif
	.d_open = labrador_open,
	.d_close = labrador_close,
	.d_ioctl = labrador_ioctl,
	.d_mmap = labrador_mmap,
};

static driver_t labrador_driver = {
	.name = "labrador",
	.methods = labrador_methods,
	.size = sizeof(struct labrador_softc),
};

#define LAB_PACKET_SIZE 750

static const struct usb_config labrador_usb_config[LAB_ENDPOINTS] = {
	[LAB_ISOC0_RD] = {
		.type = UE_ISOCHRONOUS,
		.endpoint = 0x0081,
		.direction = UE_DIR_IN,
		.frames = 15, // XXX
		.bufsize = 15 * (LAB_PACKET_SIZE + 8), // XXX
		.flags = {.short_xfer_ok = 1, .short_frames_ok = 1},
		.callback = labrador_isoc_callback,
	},
};


DRIVER_MODULE(labrador, uhub, labrador_driver, labrador_devclass, NULL, NULL);
MODULE_DEPEND(labrador, usb, 1, 1, 1);
MODULE_VERSION(labrador, 1);

static const STRUCT_USB_HOST_ID labrador_devs[] = {
	        {USB_VPI(ESPOTEK_VENDOR, ESPOTEK_LABRADOR, 0)},
};

static int
labrador_probe(device_t dev)
{
	struct usb_attach_arg *uaa = device_get_ivars(dev);

	if (uaa->usb_mode != USB_MODE_HOST)
		return (ENXIO);
	if (uaa->info.bConfigIndex != 0)
		return (ENXIO);
	if (uaa->info.bIfaceIndex != 0)
		return (ENXIO);

	return (usbd_lookup_id_by_uaa(labrador_devs, sizeof(labrador_devs), uaa));
}

static int
labrador_attach(device_t dev)
{
	struct usb_attach_arg *uaa = device_get_ivars(dev);
	struct labrador_softc *sc = device_get_softc(dev);
	int error;

	sc->sc_udev = uaa->device;
	sc->sc_unit = device_get_unit(dev);
	sc->sc_iface_index = uaa->info.bIfaceIndex;
	sc->sc_measurements.m_buffer = NULL;

	SNPRINTF(sc->sc_name, sizeof(sc->sc_name), "%s",
		device_get_nameunit(dev));

	device_set_usb_desc(dev);

	LAB_LOCK_INIT(&sc->sc_lock, "labrador lock");

	error = labrador_get_firmware_version(sc, &sc->firmware_version);
	if (error) {
		device_printf(dev, "Failed to get firmware version");
		goto detach;
	}

	error = labrador_get_firmware_variant(sc, &sc->firmware_variant);
	if (error) {
		device_printf(dev, "Failed to get firmware variant");
		goto detach;
	}

	device_printf(dev, "Firmware version %d, variant %d\n",
			sc->firmware_version, sc->firmware_variant);

	error = usbd_transfer_setup(sc->sc_udev, &sc->sc_iface_index,
			sc->sc_xfer, labrador_usb_config, LAB_ENDPOINTS,
			sc, &sc->sc_lock);
	if (error) {
		device_printf(dev, "Failed to setup USB transfer\n");
		goto detach;
	}

	LAB_LOCK(&sc->sc_lock);

#if defined(__FreeBSD__)
	error = make_dev_p(MAKEDEV_CHECKNAME | MAKEDEV_WAITOK,
			&sc->sc_cdev,
			&labrador_cdevsw,
			0,
			UID_ROOT,
			GID_OPERATOR,
			0660,
			"labrador%d.%d",
			device_get_unit(dev),
			sc->sc_iface_index);
#elif defined(__DragonFly__)
	sc->sc_cdev = make_dev(&labrador_cdevsw, 0,
	    UID_ROOT, GID_OPERATOR, 0660,
	    "labrador%d.%d", device_get_unit(dev), sc->sc_iface_index);
	if (sc->sc_cdev == NULL)
		error = ENXIO;
#else
#error "DragonFly or FreeBSD required"
#endif

	if (error != 0) {
		PRINTF("Device creation failed\n");
		LAB_UNLOCK(&sc->sc_lock);
		goto detach;
	}
	sc->sc_cdev->si_drv1 = sc;

	LAB_UNLOCK(&sc->sc_lock);

	return (0);

detach:
	labrador_detach(dev);
	return (ENXIO);
}

static int
labrador_detach(device_t dev)
{
	struct labrador_softc *sc = device_get_softc(dev);

	PRINTF("Detaching\n");

	if (sc->sc_cdev) {
		destroy_dev(sc->sc_cdev);
		sc->sc_cdev = NULL;
	}

	KASSERT(sc->sc_connected_td == NULL, "still connected");

	// Do not hold a transfer lock when calling `usbd_transfer_unsetup`!
	usbd_transfer_unsetup(sc->sc_xfer, LAB_ENDPOINTS);

	LAB_LOCK_UNINIT(&sc->sc_lock);

	return (0);
}

static void
labrador_isoc_callback(struct usb_xfer *xfer, usb_error_t error)
{
	int actlen, sumlen, aframes, nframes;
	int fr;
	usbd_xfer_status(xfer, &actlen, &sumlen, &aframes, &nframes);

	DPRINTF("isoc frame: state=%d, actlen=%d, sumlen=%d, aframes=%d, nframes=%d\n",
				USB_GET_STATE(xfer), actlen, sumlen, aframes, nframes);

	switch (USB_GET_STATE(xfer)) {
		case USB_ST_TRANSFERRED:
			if (actlen == 0) {
				goto tr_setup;
			}

			struct usb_page_cache *pc;
			struct labrador_softc *sc = usbd_xfer_softc(xfer);
			uint8_t *mbuf = sc->sc_measurements.m_buffer;
			pc = usbd_xfer_get_frame(xfer, 0);
			if (mbuf /*&& ((struct measurements_header*) mbuf)->capturing*/) {
				struct measurements_header *mhd = 
					(struct measurements_header*) mbuf;

				mhd->busy = 1;

				uint64_t max_pos = mhd->num_blocks * mhd->block_size;
				uint64_t write_pos = mhd->current_position % max_pos;
				uint8_t *data = mbuf + sizeof(struct measurements_header);

				int offset = 0;
				while (actlen > 0) {
					KASSERT(write_pos < max_pos, "Assert");
					uint64_t space = max_pos - write_pos;
					uint64_t bytes_to_write = MIN(actlen, space);

					if (write_pos + bytes_to_write > max_pos) {
						PRINTF("ERROR\n");
						break;
					}

					usbd_copy_out(pc, offset, data + write_pos, bytes_to_write);
					actlen -= bytes_to_write;
					offset += bytes_to_write;
					write_pos += bytes_to_write;
					write_pos = write_pos % max_pos;
					mhd->total_bytes += bytes_to_write;
				}

				mhd->current_position = write_pos;
				mhd->busy = 0;
			}
			usbd_transfer_start(sc->sc_xfer[0]);
			break;

		case USB_ST_SETUP:
tr_setup:
			for (fr = 0; fr < aframes; fr++) {
				usbd_xfer_set_frame_len(xfer, fr, LAB_PACKET_SIZE);
			}

			usbd_transfer_submit(xfer);
			break;

		default:
			DPRINTF("error=%s\n", usbd_errstr(error));
	}
}

#if defined(__FreeBSD__)
static int
labrador_open(struct cdev *dev, int oflags __unused, int devtype __unused,
		struct thread *td)
{
#elif defined(__DragonFly__)
static int
labrador_open(struct dev_open_args *ap)
{
	struct cdev *dev = ap->a_head.a_dev;
	struct thread *td = curthread;
#endif
	struct labrador_softc *sc = dev->si_drv1;

	PRINTF("OPEN\n");

	LAB_LOCK(&sc->sc_lock);

	if (sc->sc_connected_td == NULL) {
		sc->sc_connected_td = td;
	} else {
		LAB_UNLOCK(&sc->sc_lock);
		return (ENXIO);
	}

	LAB_UNLOCK(&sc->sc_lock);
	return (0);
}



#if defined(__FreeBSD__)
static int
labrador_close(struct cdev *dev, int fflag __unused, int devtype __unused,
		struct thread *td)
{
#elif defined(__DragonFly__)
static int
labrador_close(struct dev_close_args *ap)
{
	struct cdev *dev = ap->a_head.a_dev;
	struct thread *td __unused = curthread;
#endif
	struct labrador_softc *sc = dev->si_drv1;
	PRINTF("CLOSE\n");

	// XXX: It's kindof safe to not acquire the lock here
	// as any new call to `open` would be rejected as
	// `sc_connected_td` is still != NULL.

	// NOTE: do not hold a transfer lock when calling this!
	usbd_transfer_drain(sc->sc_xfer[LAB_ISOC0_RD]);

	// At this point, all USB transfers have been stopped.
	// XXX: There is a slight race condition here, as 
	// you could call `labrador_start_isoc` from a different process
	// and this would start transfers again. As we do not hold the
	// lock for a short moment, we cannot avoid that.
	// XXX: Add a `is_closing` flag.

	LAB_LOCK(&sc->sc_lock);

	// XXX: what happens when we `dup` the file descriptor
	// pointing to the device?
	KASSERT(sc->sc_connected_td == td, "wrong thread");

	if (sc->sc_measurements.m_buffer != NULL) {
		FREE(sc->sc_measurements.m_buffer, M_DEVBUF);
		sc->sc_measurements.m_buffer = NULL;
	}

	sc->sc_connected_td = NULL;

	LAB_UNLOCK(&sc->sc_lock);

	return (0);
}


#if defined(__FreeBSD__)
static int
labrador_ioctl(struct cdev *dev, u_long cmd, caddr_t addr, int flag __unused, struct thread *td __unused)
{
#elif defined(__DragonFly__)
static int
labrador_ioctl(struct dev_ioctl_args *ap)
{
	struct cdev *dev = ap->a_head.a_dev;
	u_long cmd = ap->a_cmd;
	caddr_t addr = ap->a_data;
	//int flag = ap->a_fflag;
#endif
	struct labrador_softc *sc = dev->si_drv1;
	int error = 0;

	LAB_LOCK(&sc->sc_lock);
	switch (cmd) {
	case LABRADOR_SET_DEVICE_MODE:
		error = labrador_set_device_mode(sc,
				((struct labrador_device_mode*)addr)->mode,
				((struct labrador_device_mode*)addr)->gain_mask);
		break;
	case LABRADOR_SET_PSU:
		error = labrador_set_psu(sc,
				*((uint16_t*)addr));
		break;
	case LABRADOR_SET_DIGITAL_PINS:
		error = labrador_set_digital_pins(sc, *((uint16_t*)addr));
		break;

	case LABRADOR_SET_FGEN:
		error = labrador_set_fgen(sc, (struct labrador_fgen_data*)addr);
		break;


	case LABRADOR_ALLOC_BUFFERS:
		error = labrador_alloc_buffers(sc, (struct labrador_alloc_buffers*)addr);
		break;

	case LABRADOR_START_ISOC:
		error = labrador_start_isoc(sc);
		if (error == 0)
			usbd_transfer_start(sc->sc_xfer[LAB_ISOC0_RD]);

		break;

	default:
		error = ENOTTY;
		break;
	}
	LAB_UNLOCK(&sc->sc_lock);

	return (error);
}

#if defined(__FreeBSD__)
static int
labrador_mmap(struct cdev *dev, vm_ooffset_t offset, vm_paddr_t *paddr,
    int nprot, vm_memattr_t *memattr __unused)
{
#elif defined(__DragonFly__)
static int
labrador_mmap(struct dev_mmap_args *ap)
{
	struct cdev *dev = ap->a_head.a_dev;
	vm_offset_t offset = ap->a_offset;
	int nprot = ap->a_nprot;
#endif
	struct labrador_softc *sc = dev->si_drv1;

	PRINTF("mmap called: offset=%ld nprot=%d\n", offset, nprot);

	LAB_LOCK(&sc->sc_lock);

	if (sc->sc_measurements.m_buffer == NULL) {
		LAB_UNLOCK(&sc->sc_lock);
		return (EINVAL);
	}

	if (offset + PAGE_SIZE > sc->sc_measurements.m_size) {
		LAB_UNLOCK(&sc->sc_lock);
		return (EINVAL);
	}

	if (nprot != PROT_READ) {
		PRINTF("Only read-only mmap allowed\n");
		LAB_UNLOCK(&sc->sc_lock);
		return (EINVAL);
	}

#if defined(__FreeBSD__)
	*paddr = vtophys(sc->sc_measurements.m_buffer + offset);
#elif defined(__DragonFly__)
	ap->a_result = atop(vtophys(sc->sc_measurements.m_buffer + offset));
#endif

	LAB_UNLOCK(&sc->sc_lock);

	return (0);
}


static int
labrador_send_control(struct labrador_softc *sc,
		uint8_t request_type,
		uint8_t request,
		uint16_t value,
		uint16_t index,
		uint16_t length,
		uint8_t *data)
{
	int error;

	struct usb_device_request req;

	req.bmRequestType = request_type;
	req.bRequest = request;
	USETW(req.wValue, value);
	USETW(req.wIndex, index);
	USETW(req.wLength, length);

	error = usbd_do_request(sc->sc_udev, NULL, &req, data);

	if (error) {
		return (ENXIO);
	}
	return (0);
}

static int
labrador_get_firmware_version(struct labrador_softc *sc, uint16_t *firmver)
{
	return labrador_send_control(sc,
			UT_READ_VENDOR_DEVICE,
			LAB_CMD_GET_FIRMWARE_VERSION,
			0, 0, 2, (uint8_t*)firmver);
}

static int
labrador_get_firmware_variant(struct labrador_softc *sc, uint8_t *firmvar)
{
	return labrador_send_control(sc,
			UT_READ_VENDOR_DEVICE,
			LAB_CMD_GET_FIRMWARE_VARIANT,
			0, 0, 1, firmvar);
}

static int
labrador_cmd(struct labrador_softc *sc, uint8_t cmd, uint16_t value, uint16_t index)
{
	return labrador_send_control(sc,
			UT_WRITE_VENDOR_DEVICE,
			cmd,
			value, index, 0, NULL);
}

static int
labrador_set_psu(struct labrador_softc *sc, uint16_t duty_psu)
{
	PRINTF("set_psu: %d\n", duty_psu);
	return labrador_cmd(sc, LAB_CMD_SET_PSU, duty_psu, 0);
}

static int
labrador_set_device_mode(struct labrador_softc *sc, uint16_t mode, uint16_t gain_mask)
{
	PRINTF("set_device_mode: mode=%d, gain_mask=%d\n", mode, gain_mask);
	return labrador_cmd(sc, LAB_CMD_SET_DEVICE_MODE, mode, gain_mask);
}

static int
labrador_set_digital_pins(struct labrador_softc *sc, uint16_t state)
{
	PRINTF("set_digital_pins: %d\n", state);
	return labrador_cmd(sc, LAB_CMD_SET_DIGITAL_PINS, state, 0);
}

static int
labrador_set_fgen(struct labrador_softc *sc, struct labrador_fgen_data *fgen)
{
	uint8_t cmd;

	PRINTF("set_fgen\n");

	if (fgen->n_samples > 512)
		return (EINVAL);

	switch (fgen->channel) {
		case LABRADOR_FGEN_CH1:
			cmd = LAB_CMD_SET_FGEN_WAVEFORM_CH1;
			break;
		case LABRADOR_FGEN_CH2:
			cmd = LAB_CMD_SET_FGEN_WAVEFORM_CH2;
			break;
		default:
			return (EINVAL);
	}

	return labrador_send_control(sc,
			UT_WRITE_VENDOR_DEVICE,
			cmd,
			fgen->timer_period,
			fgen->clock_setting,
			fgen->n_samples,
			fgen->samples);
}

static int
labrador_alloc_buffers(struct labrador_softc *sc, struct labrador_alloc_buffers *alloc_bufs) {

	if (sc->sc_measurements.m_buffer != NULL) {
		// we already have a buffer allocated.
		return (EINVAL);
	}

	// XXX: These limits are randomly chosen!
	if (alloc_bufs->num_bufs < 30) {
		alloc_bufs->num_bufs = 30;
	}

	if (alloc_bufs->num_bufs > 1000) {
		alloc_bufs->num_bufs = 1000;
	}

	size_t sz = roundup(
			(alloc_bufs->num_bufs * LAB_PACKET_SIZE) +
			sizeof(struct measurements_header),
			PAGE_SIZE); 

	PRINTF("Total allocated size: %ld\n", sz);
	KASSERT(sz >= PAGE_SIZE, "size must be at least one page");

	// we shouldn't block while holding the lock
#
#if defined(__FreeBSD__)
	void *buf = malloc(sz, M_DEVBUF, M_WAITOK | M_ZERO);
#elif defined(__DragonFly__)
	void *buf = kmalloc(sz, M_DEVBUF, M_WAITOK | M_ZERO);
#endif
	if (buf == NULL) {
		PRINTF("failed to allocate\n");
		alloc_bufs->num_bufs = 0;
		alloc_bufs->map_size = 0;
		return (ENOMEM);
	}

	alloc_bufs->map_size = sz; // XXX

	sc->sc_measurements.m_buffer = (uint8_t*) buf;
	sc->sc_measurements.m_num_bufs = alloc_bufs->num_bufs;
	sc->sc_measurements.m_size = alloc_bufs->map_size;

	// initialize the measurements_header
	struct measurements_header *mhd = (struct measurements_header*) buf;
	mhd->current_position = 0;
	mhd->busy = 0;
	mhd->capturing = 1;
	mhd->num_blocks = sc->sc_measurements.m_num_bufs;
	mhd->block_size = LAB_PACKET_SIZE;

	return (0);
}


static int
labrador_start_isoc(struct labrador_softc *sc)
{
	PRINTF("start_isoc\n");
	return labrador_cmd(sc, LAB_CMD_START_ISOC, 0, 0);
}
